import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 *
 * References:
 *
 * https://www.baeldung.com/java-hashcode
 */
public class Lfraction implements Comparable<Lfraction> {

   /**
    * Main method. Different tests.
    */
   public static void main(String[] param) {
   }

   private long numerator;
   private long denominator;

   /**
    * Constructor.
    *
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction(long a, long b) {

      int parsedNumerator = Math.abs((int) a);
      int parsedDenominator = Math.abs((int) b);
      int upperLimit = 0;
      int commonFactor = 1;

      if (b != 0) {
         if (a == 0){
            b = 1;
         } else

         upperLimit = Math.min(parsedNumerator, parsedDenominator);
         for (int i = 1; i <= upperLimit; i++){
            if ((parsedNumerator % i  == 0 && parsedDenominator % i == 0)){
               if (commonFactor < i) commonFactor = i;
            }
         }

         a = a / commonFactor;
         b = b / commonFactor;

         if (b < 0){
            this.numerator = -a;
            this.denominator = -b;
         } else {
            this.numerator = a;
            this.denominator = b;
         }
      } else throw new RuntimeException("Construction of a fraction with denominator being 0");

   }

   /**
    * Public method to access the numerator field.
    *
    * @return numerator
    */
   public long getNumerator() {
      return this.numerator;
   }

   /**
    * Public method to access the denominator field.
    *
    * @return denominator
    */
   public long getDenominator() {
      return this.denominator;
   }

   /**
    * Conversion to string.
    *
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      String fractionExpressionToString = "";

      fractionExpressionToString = this.numerator + "/" + this.denominator;

      return fractionExpressionToString;
   }

   /**
    * Equality test.
    *
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals(Object m) {
      return this.compareTo((Lfraction) m) == 0;
   }

   /**
    * Hashcode has to be equal for equal fractions.
    *
    * @return hashcode
    */
   @Override
   public int hashCode() {
      int hash = 7;
      hash = 31 * hash + (int) this.numerator;
      hash = 31 * hash + (int) this.denominator;
      return hash;
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {

      long resultFractionDenominator;
      long resultFractionNumerator;

      resultFractionDenominator = this.denominator * m.denominator;
      resultFractionNumerator = (this.numerator * m.denominator) + (m.numerator * this.denominator);


      return new Lfraction(resultFractionNumerator, resultFractionDenominator);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      long resultFractionDenominator;
      long resultFractionNumerator;

      resultFractionDenominator = this.denominator * m.denominator;
      resultFractionNumerator = this.numerator * m.numerator;


      return new Lfraction(resultFractionNumerator, resultFractionDenominator);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {

      if (this.numerator != 0) {
         if (this.numerator < 0) return new Lfraction(-this.denominator, -this.numerator);
         else return new Lfraction(this.denominator, this.numerator);
      }
      else throw new RuntimeException("Inverse of fraction could have a denominator of 0");
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(-numerator, denominator);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      return this.plus(m.opposite());
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {


      try{
         return this.times(m.inverse());
      } catch (RuntimeException e){
         throw new RuntimeException("Division causes the fractions numerator to become due to inverse");
      }
   }

   public Lfraction pow(int num){

      if (num < 0){
         return this.pow(-num).inverse();
      } else if (num == 0){
         return new Lfraction(1,1);
      } else {
         return this.times(this.pow(num - 1));
      }
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {

      Lfraction tmp;
      tmp = this.minus(m);

      if (tmp.numerator == 0) return 0;

      else if (tmp.numerator < 0) return -1;

      else return 1;
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(numerator, denominator);
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return this.numerator / this.denominator;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {

      Lfraction integerPart = new Lfraction(this.integerPart(), 1);

      return this.minus(integerPart);
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      double fractionNumeratorAsDouble = this.numerator;
      double fractionDenominatorAsDouble = this.denominator;
      return fractionNumeratorAsDouble / fractionDenominatorAsDouble;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {

      return new Lfraction(Math.round(f * d), d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      long numerator = 0;
      long denominator = 1;

      String[] stringSplitByChar = s.split("/");

      if (stringSplitByChar.length != 2) {
         throw new RuntimeException("Invalid input for fraction: " + s + "Should be numerator/denominator");
      }

      try{
         numerator = Long.parseLong(stringSplitByChar[0]);
         denominator = Long.parseLong(stringSplitByChar[1]);
      } catch (IllegalArgumentException e){
         System.out.println("Unable to convert to long at: " + s);
      }

      return new Lfraction(numerator, denominator);
   }
}

